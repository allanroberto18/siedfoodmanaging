angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ion-datetime-picker', 'chart.js'])

  .run(function ($ionicPlatform, $ionicPickerI18n) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      $ionicPickerI18n.weekdays = ["DO", "SE", "TE", "QA", "QI", "SE", "SA"];
      $ionicPickerI18n.months = ["Janeiro", "Fevereiro", "Março", "Abrio", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
      $ionicPickerI18n.ok = "Ok";
      $ionicPickerI18n.cancel = "Cancelar";
      $ionicPickerI18n.okClass = "button-balanced";
      $ionicPickerI18n.cancelClass = "button-balanced";
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, ChartJsProvider) {
    $stateProvider.state('index', {
      url : '/',
      templateUrl : 'index.html',
      controller : 'InicioCtrl'
    });

    ChartJsProvider.setOptions({colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']});

    $urlRouterProvider.otherwise("/");
  });
