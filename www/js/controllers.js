angular.module('starter.controllers', [])
  .controller('InicioCtrl', function ($scope, $ionicPlatform, $ionicPopup, $log) {
    $scope.titulo = 'Managing 2017';

    $ionicPlatform.registerBackButtonAction(function (event) {
      $scope.ConfirmarSaida();
    }, 100);

    $scope.ConfirmarSaida = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Atenção',
        template: 'Tem certeza que deseja sair do sistema?'
      });

      confirmPopup.then(function(res) {
        if(res) {
          ionic.Platform.exitApp();
        }
      });
    };

  })

  .controller('CaixaCtrl', function ($scope, $ionicLoading, $ionicModal, $ionicScrollDelegate,
                                     $log, $state, ClientAPI, TratarData, TratarObjetos, TratarFloat) {

    $scope.titulo = 'Relação de Caixas';

    $scope.dias = '';

    $scope.listagem = true;

    $scope.datahora = '';

    $scope.dataInicial = '';

    $scope.movimento = '';

    $scope.resumoMovimento = '';

    $scope.resumoVendas = '';

    $scope.balancete = '';

    $scope.cancelamentos = '';

    $scope.totalCancelamentos = '';

    $scope.dados = {};

    $ionicModal.fromTemplateUrl('templates/caixa.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.ProcessarRelatorio = function (dias) {
      $scope.dias = dias;

      $scope.GetTurno();

      $scope.modal.hide();
    };

    $scope.GetTurno = function () {
      $scope.listagem = true;

      $ionicLoading.show();

      var parametro = 'GetTurno/' + $scope.dias;

      ClientAPI.retornarGet(parametro)
        .then(function (response) {
          $scope.dados = response.data.result;

          $ionicLoading.hide();
        })
    };

    $scope.init = function (dias) {
      if (dias === '') {
        dias = 1;
      }
      $scope.dias = dias;

      $scope.GetTurno();
    };

    $scope.GerarRelatorio = function (caixa) {
      $scope.listagem = false;

      $ionicLoading.show();

      $scope.caixa = caixa;

      $scope.datahora = TratarData.RetornarDataHora();

      ClientAPI.retornarGet('GetMovimentoDoTurno/' + caixa.ID_CAIXA + '/' + caixa.TURNO)
        .then(function (response) {
          $scope.movimento = response.data.result;

          ClientAPI.retornarGet('GetResumoDoMovimentoDoTurno' + '/' + caixa.ID_CAIXA + '/' + caixa.TURNO)
            .then(function (response) {
              $scope.resumoMovimento = response.data.result;
            });

          ClientAPI.retornarGet('GetResumoDaVendaDoTurno' + '/' + caixa.ID_CAIXA + '/' + caixa.TURNO)
            .then(function (response) {
              $scope.resumoVendas = response.data.result;
            });

          ClientAPI.retornarGet('GetBalanceteDoTurno' + '/' + caixa.ID_CAIXA + '/' + caixa.TURNO)
            .then(function (response) {
              $scope.balancete = response.data.result;
            });

          ClientAPI.retornarGet('GetCancelamentoNoTurno' + '/' + caixa.ID_CAIXA + '/' + caixa.TURNO)
            .then(function (response) {
              var dados = response.data.result;
              var count = TratarObjetos.ContarObjetos(dados[0]);
              var total = 0;
              var valor = 0;
              var i = 0;
              while (i < count) {
                valor = TratarFloat.ConverterParaFloat(dados[0][i].VALOR);
                total = total + valor;
                i++;
              }

              $scope.cancelamentos = response.data.result;

              $scope.totalCancelamentos = TratarFloat.ConverterParaString(total);
            });

          $ionicScrollDelegate.scrollBottom();

          $ionicLoading.hide();
        });
    };
  })

  .controller('VendasCtrl', function ($scope, $ionicLoading, $ionicModal, $ionicScrollDelegate, $log,
                                      ClientAPI, TratarData, TratarObjetos, TratarFloat) {
    $scope.labels = [];
    $scope.series = [];
    $scope.data = [];
    $scope.ano = '';
    $scope.totalAno = 0;
    $scope.colors = ["rgb(159,204,0)"];
    $scope.titulo = 'Relatório de Vendas';
    $scope.relatorio = false;
    $scope.mostrarRodape = false;
    $scope.tipo = '';
    $scope.dataInicial = '';
    $scope.dataFinal = '';
    $scope.dados = '';
    $scope.rodape = {};

    $scope.init = function () {
      $scope.dataInicial = TratarData.SubtrairDataSemFormato(1);
      $scope.dataFinal = new Date();

      $scope.tipo = '';
    };

    $ionicModal.fromTemplateUrl('templates/vendas.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.RefazerRelatorio = function (dataInicial, dataFinal) {
      $scope.tipo = '';

      if ($scope.dados === '') {
        return;
      }
    }

    $scope.GetVenda = function (dataInicial, dataFinal, tipo) {
      $ionicLoading.show();

      $scope.tipo = tipo;

      switch (tipo) {
        default:
          if (dataInicial === '') {
            $scope.relatorio = false;

            $scope.labels = [];
            $scope.data = [];

            $ionicLoading.hide();
            return;
          }

          var d1 = TratarData.formatarDataHora(dataInicial);
          var d2 = TratarData.formatarDataHora(dataFinal);

          var metodo = '';

          switch (tipo) {
            case 1 :
              metodo = 'GetVenda';
              break;
            case 2 :
              metodo = 'GetVendaPorProduto';
              break;
            case 3 :
              metodo = 'GetVendaPorGrupoProduto';
              break;
            case 4 :
              metodo = 'GetVendaGraficoPorVendedor';

              var l1 = TratarData.formatarData(dataInicial);
              var l2 = TratarData.formatarData(dataFinal);

              $scope.series = ['Venda R$'];

              break;
          }

          ClientAPI.retornarGet(metodo + '/' + d1 + '/' + d2)
            .then(function (response) {

              $scope.series = ['Ano'];

              $scope.dados = response.data.result;

              if (tipo == 4) {
                var i = 0;
                var count = TratarObjetos.ContarObjetos($scope.dados[0]);
                while (i < count) {
                  $scope.data[[i]] = TratarFloat.ConverterParaFloat($scope.dados[0][i].Venda);
                  $scope.labels[i] = $scope.dados[0][i].Vendedor.substring(0, 3);
                  i++;
                }
              }
            });

          $scope.GerarRodape(d1, d2);
          break;
        case 5:
          $scope.modal.show();
          break;
      }
      $scope.relatorio = true;

      $ionicLoading.hide();
    };

    $scope.GerarGraficoPorAno = function (ano) {
      $scope.labels = [];
      $scope.data = [];

      $scope.mostrarRodape = false;

      $scope.ano = ano;

      ClientAPI.retornarGet('GetVendaGraficoAnual' + '/' + ano)
        .then(function (response) {
          $scope.dados = response.data.result;

          var valor = 0;
          var total = 0;
          var count = TratarObjetos.ContarObjetos($scope.dados[0]);
          var i = 0;
          while (i < count) {
            $scope.data[[i]] = TratarFloat.ConverterParaFloat($scope.dados[0][i].Valor);
            $scope.labels[i] = $scope.dados[0][i].Mes.substring(0, 3);

            valor = TratarFloat.ConverterParaFloat($scope.dados[0][i].Valor);
            total = total + valor;
            i++;
          }

          $scope.totalAno = TratarFloat.ConverterParaString(total);
        });

      $scope.modal.hide();

      $scope.relatorio = true;

      $ionicScrollDelegate.scrollBottom();

      $ionicLoading.hide();
    }

    $scope.scrollTop = function () {
      $ionicScrollDelegate.scrollTop();
    };

    $scope.GerarRodape = function (dataInicial, dataFinal) {

      ClientAPI.retornarGet('GetCalculaRodapeVenda' + '/' + dataInicial + '/' + dataFinal)
        .then(function (response) {
          $scope.rodape = response.data.result;

          $scope.mostrarRodape = true;

          $ionicScrollDelegate.scrollBottom();
        });
    };

  })

  .controller('ConsumoCtrl', function ($scope, $log, $ionicLoading, $ionicScrollDelegate, ClientAPI, TratarData,
                                       TratarObjetos, TratarFloat) {
    $scope.titulo = 'Vendas em Consumo';

    $scope.dataInicial = '';
    $scope.dataFinal = '';
    $scope.relatorio = false;
    $scope.dados = [];
    $scope.rodape = [];

    $scope.init = function () {
      $scope.dataInicial = TratarData.SubtrairDataSemFormato(1);

      $scope.dataFinal = new Date();
    };

    $scope.GerarRelatorio = function (dataInicial, dataFinal) {
      $ionicLoading.show();

      var d1 = TratarData.formatarDataHora(dataInicial);
      var d2 = TratarData.formatarDataHora(dataFinal);

      ClientAPI.retornarGet('GetVendaEmConsumo/' + d1 + '/' + d2)
        .then(function (response) {
          $scope.rodape = response.data.result;
        });

      ClientAPI.retornarGet('GetVendaFechada/' + d1 + '/' + d2)
        .then(function (response) {
          $scope.relatorio = true;

          $scope.dados = response.data.result;

          $ionicScrollDelegate.scrollBottom();

          $ionicLoading.hide();
        });
    };
  })

  .controller('EstoqueCtrl', function ($scope, $ionicLoading, $ionicScrollDelegate, $log, ClientAPI) {
    $scope.titulo = 'Controle de Estoque';

    $scope.relatorio = false;
    $scope.tipo = 0;
    $scope.grupo = 0;
    $scope.dados = [];
    $scope.grupos = [];

    $scope.LoadGrupos = function () {
      $ionicLoading.show();

      ClientAPI.retornarGet('GetGrupoDeProduto')
        .then(function (response) {
          $scope.grupos = response.data.result;
          $scope.grupos[0].unshift({Codigo: 0, Grupo: 'Todos'});

          $ionicLoading.hide();
        });
    };

    $scope.init = function () {
      $scope.LoadGrupos();
    };

    $scope.GerarRelatorio = function (grupo, tipo) {
      $scope.relatorio = false;
      $scope.dados = [];

      $ionicLoading.show();

      var tipoConsulta = '';
      switch (tipo) {
        case 0 :
          tipoConsulta = 'EstoqueZero';
          break;
        case 1 :
          tipoConsulta = 'EstoqueZero';
          break;
        case 2 :
          tipoConsulta = 'AbaixoEstoqueMinimo';
          break;
        case 3 :
          tipoConsulta = 'AbaixoPontoPedido';
          break;
        case 4 :
          tipoConsulta = 'AbaixoEstoquePadrao';
          break;
      }

      ClientAPI.retornarGet('GetEstoque/' + grupo + '/' + tipoConsulta)
        .then(function (response) {
          $scope.relatorio = true;

          $scope.dados = response.data.result;

          $ionicLoading.hide();

          $ionicScrollDelegate.scrollBottom();
        });
    };

  })

  .controller('CancelamentosCtrl', function ($scope, $ionicLoading, $ionicScrollDelegate, $ionicPopup, $log, ClientAPI, TratarObjetos) {
    $scope.titulo = 'Cancelamento de itens';

    $scope.relatorio = false;
    $scope.mesas = [];
    $scope.cartoes = [];
    $scope.comandas = [];

    $scope.comanda = '';
    $scope.item = '';
    $scope.index = '';
    $scope.tipo = '';

    $scope.init = function () {
      $scope.CarregarItemsEmMovimento();
    };

    $scope.CarregarItemsEmMovimento = function()
    {
      $scope.RetornaMesasEmMovimento();

      $scope.RetornaCartoesEmMovimento();

      $scope.relatorio = true;

      $ionicScrollDelegate.scrollBottom();
    };

    $scope.RetornaMesasEmMovimento = function()
    {
      $ionicLoading.show();

      ClientAPI.retornarGet('GetRetornaMesasEmMovimento')
        .then(function(response) {
          $scope.mesas = response.data.result;

          $ionicLoading.hide();
        });
    };

    $scope.RetornaCartoesEmMovimento = function()
    {
      $ionicLoading.show();

      ClientAPI.retornarGet('GetRetornaCartoesEmMovimento')
        .then(function(response) {

          $scope.cartoes = response.data.result;

          $ionicLoading.hide();
        });
    };

    $scope.RetornaItemsDaComanda = function(comanda, tipo)
    {
      $scope.comanda = comanda;
      $scope.tipo = tipo;

      $ionicLoading.show();

      ClientAPI.retornarGet('GetRetornaItensDaComanda/' + comanda + '/' + tipo)
        .then(function(response){
          $scope.comandas = response.data.result;

          $ionicLoading.hide();

          $ionicScrollDelegate.scrollTop();

          $scope.relatorio = false;
        });
    };

    $scope.Voltar = function()
    {
      $ionicLoading.show();

      $scope.comandas = [];
      $scope.comanda = '';
      $scope.tipo = '';
      $scope.item = '';
      $scope.index = '';

      $scope.relatorio = true;

      $scope.CarregarItemsEmMovimento();

      $ionicScrollDelegate.scrollTo();

      $ionicLoading.hide();
    };

    $scope.EnviarCancelamento = function(item, index) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Atenção',
        template: 'Deseja excluir esse item?'
      });
      confirmPopup.then(function(res) {
        if(res) {
          $scope.item = item;

          $scope.index = index;

          $scope.RemoverItemDaComanda();
        }
      });
    };

    $scope.RemoverItemDaComanda = function()
    {
      $ionicLoading.show();

      $scope.comandas[0].splice($scope.index, 1);

      ClientAPI.retornarGet('GetCancelaItemDaComanda/' + $scope.item + '/' + $scope.comanda + '/' + $scope.tipo)
        .then(function(respnse) {
          $scope.item = '';
          $scope.index = '';

          $ionicLoading.hide();

        });
    };
  })

  .controller('FinanceiroCtrl', function ($scope, $http) {
    $scope.titulo = 'Financeiro';
  })

  .controller('ComprasCtrl', function ($scope, $http) {
    $scope.titulo = 'Compras';
  })
;
